# COVID-19 vaccines

[Search for relevant information about COVID-19](https://www.google.com/search?q=covid-19)

## AZD1222: Oxford-AstraZeneca vaccine

* Developed by:
  * University of Oxford - England, UK
  * AstraZeneca - British-Swedish company
* Storage: 2 to 8°C
* Cost: USD 3 to 4
* Efficiency: 90% as of November 2020

### Design

SARS-CoV-2 spike protein using the modified adenovirus ChAdOx1

See [§How an adenovirus-based vaccine works](#how-an-adenovirus-based-vaccine-works).

### Read more

* [Wikipedia](https://en.wikipedia.org/wiki/AZD1222)
* [How the Oxford-AstraZeneca Vaccine Works](https://www.nytimes.com/interactive/2020/health/oxford-astrazeneca-covid-19-vaccine.html)


## mRNA-1273: Moderna vaccine

* Developed by:
  * NIAID, Federal government of the United States
  * BARDA, Federal government of the United States
  * Moderna - US biotechnology company
* Efficacy: 94.1%
* Storage temperature: -20°C
* Cost: Lowest: USD 15 for the US

### Design

See [§How a modRNA vaccine works](#how-a-modrna-vaccine-works)

### Read more

* [Wikipedia](https://en.wikipedia.org/wiki/MRNA-1273)
* [New York Times: How Moderna's Covid-19 Vaccine Works](https://www.nytimes.com/interactive/2020/health/moderna-covid-19-vaccine.html)

## Tozinameran: Pfizer-BioNTech vaccine

* Developed by:
  * BioNTech - German biotechnology company based in Mainz
  * Pfizer - US pharmaceutical corporation
* Cost: As low as USD 10 discounted for African health workers
* Storage temperature: -80 to -60°C
* Effectiveness: 95% as of November 2020

### Design

See [§How a modRNA vaccine works](#how-a-modrna-vaccine-works).

Uses pseudouridylyl instead of uracil.

### Read more

* [Wikipedia](https://en.wikipedia.org/wiki/Pfizer-BioNTech_COVID-19_vaccine)
* [New York Times: How the Pfizer-BioNTech Vaccine Works](https://www.nytimes.com/interactive/2020/health/pfizer-biontech-covid-19-vaccine.html)
* [Reverse Engineering the source code of the BioNTech/Pfizer SARS-CoV-2 Vaccine](https://berthub.eu/articles/posts/reverse-engineering-source-code-of-the-biontech-pfizer-vaccine/)


## Gam-COVID-Vac / Sputnik V: Gamaleya

* Developed by: Gamaleya Research Institute of Epidemiology and Microbiology - Russian medical research institute
* Storage temperature: -18°C
* Efficacy (self-reported): 91.4%

### Design

Complementary DNA stored in two different adenoviruses: Ad26 and Ad5.

See [§How an adenovirus-based vaccine works](#how-an-adenovirus-based-vaccine-works).

### Read more

* [New York Times: How Gamaleya’s Vaccine Works](https://www.nytimes.com/interactive/2021/health/gamaleya-covid-19-vaccine.html)


## CoronaVac: Sinovac

* Developed by: Sinovac - Chinese biopharmaceutical company
* Storage temperature: 2 to 8°C
* Efficacy:
  * Brazil: 50.38% as of January 2021
  * Turkey: 91.25%
  * Indonesia: 65.3%
* Cost:
  * As low as USD 10.3 in Brazil
  * IDR 200,000 in Indonesia

### Design

Virus sampled in China.

See [§How an inactivated virus vaccine works](#how-an-inactivated-virus-vaccine-works).


### Read more

* [Wikipedia](https://en.wikipedia.org/wiki/CoronaVac)
* [How the Sinovac Vaccine Works](https://www.nytimes.com/interactive/2020/health/sinovac-covid-19-vaccine.html)


## BBIBP-CorV: Sinopharm

* Developed by: CNPGC (Sinopharm) - Chinese state-owned enterprise
* Storage temperature: 
* Efficacy (Sinopharm): 79.34%
* Cost: 

### Design

Three variants obtained from Chinese hospitals.

See [§How an inactivated virus vaccine works](#how-an-inactivated-virus-vaccine-works).

### Read more

* [Wikipedia](https://en.wikipedia.org/wiki/BBIBP-CorV)
* [New York Times: How the Sinopharm Vaccine Works](https://www.nytimes.com/interactive/2020/health/sinopharm-covid-19-vaccine.html)



## How an adenovirus-based vaccine works

* DNA protected inside adenovirus
* Cells engulf the adenovirus
* Adenovirus pushes DNA into the nucleus
* Nucleus reads DNA into mRNA
* Cells build spikes from instructions in mRNA
* The immune system detects the spikes and prepares for it

## How a modRNA vaccine works

Nucleoside-modified mRNA protected by lipid bubbles
  * Bubbles combine with cells and release the mRNA
  * Cells build spikes from instructions in mRNA
  * The immune system detects the spikes and prepares for it

## How an inactivated virus vaccine works

Preparation:
* SARS-CoV-2 viruses are grown on monkey kidney cells
* Beta-propiolactone bonds to the genes of the virus, disabling them.
  * Proteins including spikes remain intact

Post-injection:
* The immune system detects the spikes and prepares for it
